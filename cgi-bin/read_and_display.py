#!/usr/bin/python

# this file must be executable, use command chmod +x <filename>
# this file must be in cgi-bin directory

import csv

input_file1 = open('cgi-bin/study.csv')
data1 = csv.DictReader(input_file1)  # read using DictReader
count_Phy_1 = 0;
count_Math_1 = 0;
count_English_1= 0;
for row in data1:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Physics':
        count_Phy_1 = count_Phy_1 + 1;
    if row['Subject'] == 'Math':
        count_Math_1 = count_Math_1 + 1;
    if row['Subject'] == 'English':
        count_English_1 = count_English_1 + 1;
input_file1.close()

input_file2 = open('cgi-bin/study2.csv')
data2 = csv.DictReader(input_file2)  # read using DictReader
count_Phy_2 = 0;
count_Math_2 = 0;
count_English_2= 0;
for row in data2:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Physics':
        count_Phy_2 = count_Phy_2 + 1;
    if row['Subject'] == 'Math':
        count_Math_2 = count_Math_2 + 1;
    if row['Subject'] == 'English':
        count_English_2 = count_English_2 + 1;
input_file2.close()

input_file3 = open('cgi-bin/study3.csv')
data3 = csv.DictReader(input_file3)  # read using DictReader
count_Phy_3 = 0;
count_Math_3 = 0;
count_English_3= 0;
for row in data3:
    # print row['Date'], row['Subject']
      if row['Subject'] == 'Physics':
        count_Phy_3 = count_Phy_3 + 1;
      if row['Subject'] == 'Math':
        count_Math_3 = count_Math_3 + 1;
      if row['Subject'] == 'English':
        count_English_3 = count_English_3 + 1;
input_file3.close()


input_file4 = open('cgi-bin/study4.csv')
data4 = csv.DictReader(input_file4)  # read using DictReader
count_Phy_4 = 0;
count_Math_4 = 0;
count_English_4= 0;
for row in data4:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Physics':
        count_Phy_4 = count_Phy_4 + 1;
    if row['Subject'] == 'Math':
        count_Math_4 = count_Math_4 + 1;
    if row['Subject'] == 'English':
        count_English_4= count_English_4 + 1;
input_file4.close()


input_file5 = open('cgi-bin/study5.csv')
data5 = csv.DictReader(input_file5)  # read using DictReader
count_Phy_5 = 0;
count_Math_5 = 0;
count_English_5= 0;
for row in data5:
    # print row['Date'], row['Subject']
    if row['Subject'] == 'Physics':
        count_Phy_5 = count_Phy_5 + 1;
    if row['Subject'] == 'Math':
        count_Math_5 = count_Math_5 + 1;
    if row['Subject'] == 'English':
        count_English_5 = count_English_5 + 1;
input_file5.close()
# print count


# HTML output
# just print out the text/html

print "Content-type:text/html\r\n\r\n"
print "<html>"
print "<head>"
print "<title>Hello</title>"
print "<style>"
print "body {"
print " background-color: #100100100;"
print "}"
print "</style>"
print "</head>"
print "<body>"
print "<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Physic &nbsp; &nbsp;&nbsp;&nbsp;Math &nbsp;&nbsp;&nbsp;&nbsp; English" "</h2>"  
print "<h2>\nAnuparp  =&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", count_Phy_1,"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ", count_Math_1,"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ", count_English_1, "</h2>"
print "<h2>\njirayud =&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", count_Phy_2,"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ", count_Math_2,"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ", count_English_2, "</h2>"
print "<h2>\nPetchada =&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", count_Phy_3,"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ", count_Math_3,"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ", count_English_3, "</h2>"
print "<h2>\nNatthawut =&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", count_Phy_4,"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ", count_Math_4,"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ", count_English_4, "</h2>"
print "<h2>\nJittipong =&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", count_Phy_5,"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ", count_Math_5,"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ", count_English_5, "</h2>"



# can use comma, do not need to use +

# In Python, we can use " or ' for string
print '<svg width="1200" height="3000">'
print '  <text x="120" y="420"> An  </text>'
print '  <text x="220" y="420"> jir </text>'
print '  <text x="320" y="420"> Pe </text>'
print '  <text x="420" y="420"> Nat </text>'
print '  <text x="520" y="420"> Jit </text>'

print '  <text x="720" y="420"> An  </text>'
print '  <text x="820" y="420"> jir </text>'
print '  <text x="920" y="420"> Pe </text>'
print '  <text x="1020" y="420"> Nat </text>'
print '  <text x="1120" y="420"> Jit </text>'

print '  <text x="420" y="820"> An  </text>'
print '  <text x="520" y="820"> jir </text>'
print '  <text x="620" y="820"> Pe </text>'
print '  <text x="720" y="820"> Nat </text>'
print '  <text x="820" y="820"> Jit </text>'
#print '  <text x="120" y="420"> ',count_Phy_1,' </text>'
#print '  <text x="220" y="420"> ',count2,' </text>'
#print '  <text x="320" y="420"> ',count3,' </text>'
#print '  <text x="420" y="420"> ',count4,' </text>'
#print '  <text x="520" y="420"> ',count5,' </text>'
print '  <text x="300" y="150"> Physics </text>'
print '  <text x="900" y="150"> Math </text>'
print '  <text x="600" y="550"> English </text>'
#graph_Physics
print ' <rect x="100" y=',400 - (20*count_Phy_1),' width= "50" height = ',(20*count_Phy_1),' style="fill: blue" />'
print ' <rect x="200" y=',400 - (20*count_Phy_2),' width= "50" height = ',(20*count_Phy_2),' style="fill: red" />'
print ' <rect x="300" y=',400 - (20*count_Phy_3),' width= "50" height = ',(20*count_Phy_3),' style="fill: green" />'
print ' <rect x="400" y=',400 - (20*count_Phy_4),' width= "50" height = ',(20*count_Phy_4),' style="fill: black" />'
print ' <rect x="500" y=',400 - (20*count_Phy_5),' width= "50" height = ',(20*count_Phy_5),' style="fill: Yellow" />'
print ' <line x1="100" y1="100" x2="100" y2="400" style="stroke:rgb(0,0,0);stroke-width:4" />'
print ' <line x1="100" y1="400" x2="600" y2="400" style="stroke:rgb(0,0,0);stroke-width:4" />'

#graph_Math
print ' <rect x="700" y=',400 - (20*count_Math_1),' width= "50" height = ',(20*count_Math_1),' style="fill: blue" />'
print ' <rect x="800" y=',400 - (20*count_Math_2),' width= "50" height = ',(20*count_Math_2),' style="fill: red" />'
print ' <rect x="900" y=',400 - (20*count_Math_3),' width= "50" height = ',(20*count_Math_3),' style="fill: green" />'
print ' <rect x="1000" y=',400 - (20*count_Math_4),' width= "50" height = ',(20*count_Math_4),' style="fill: black" />'
print ' <rect x="1100" y=',400 - (20*count_Math_5),' width= "50" height = ',(20*count_Math_5),' style="fill: Yellow" />'
print ' <line x1="700" y1="100" x2="700" y2="400" style="stroke:rgb(0,0,0);stroke-width:4" />'
print ' <line x1="700" y1="400" x2="1200" y2="400" style="stroke:rgb(0,0,0);stroke-width:4" />'

#graph_English
print ' <rect x="400" y=',800 - (20*count_English_1),' width= "50" height = ',(20*count_English_1),' style="fill: blue" />'
print ' <rect x="500" y=',800 - (20*count_English_2),' width= "50" height = ',(20*count_English_2),' style="fill: red" />'
print ' <rect x="600" y=',800 - (20*count_English_3),' width= "50" height = ',(20*count_English_3),' style="fill: green" />'
print ' <rect x="700" y=',800 - (20*count_English_4),' width= "50" height = ',(20*count_English_4),' style="fill: black" />'
print ' <rect x="800" y=',800 - (20*count_English_5),' width= "50" height = ',(20*count_English_5),' style="fill: Yellow" />'
print ' <line x1="400" y1="500" x2="400" y2="800" style="stroke:rgb(0,0,0);stroke-width:4" />'
print ' <line x1="400" y1="800" x2="900" y2="800" style="stroke:rgb(0,0,0);stroke-width:4" />'
print '</svg>'

print "</body>"
print "</html>"
